===========================================
University Nanosatellite Program Guidelines
===========================================



========================
Space Vehicle Guidelines
========================

.. req:: UNP11-1
    :id: UNP11-1

    The CubeSat shall be designed to withstand the launch and on-orbit environments of the launch vehicle without failure that results in damage of the launch vehicle and its contents or failure that causes injury to the ground handling crew.


.. req:: UNP11-2
    :id: UNP11-2

    The CubeSat should be designed to meet the requirements specified in the Calpoly CubsSat Design Specification.


