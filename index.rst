.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CAT-Sat Documentation
=====================


.. toctree::
   :maxdepth: 2
   :caption: Overview

   mission
   experiments
   solarwhite
   downlink


Introduction
============



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

