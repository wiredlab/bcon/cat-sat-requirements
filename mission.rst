===============
CAT-Sat Mission
===============


=================
Mission Narrative
=================

Our mission is to perform two different experiments, one Kennedy Space Center’s (KSC) Applied Physics Labs (APL) cryogenic thermal coating, Solar White, in space. By testing multiple coupons in a thermally controlled payload, we aim to evaluate the coating's emissivity and absorptivity properties. By monitoring temperature variations in comparison to AZ-93, which is the current industry standard for thermal coatings. Secondly, we seek to optimize radio downlink scheduling between the satellite and the ground stations of the SatNOGS network to autonomously coordinate optimal windows for downlink data to many receive-only ground stations.

		
==========
Objectives
==========

.. objective:: Solar White emissivity

    Test the emissive properties of Solar White paint.


.. objective:: Solar White comparison

    Compare the emissive properties of Solar White paint to AZ-93.


.. objective:: Solar White degredation

    Test the degradation of Solar White paint.


.. objective:: Solar White

    Compare the degradation of Solar White paint to AZ-93.


.. objective:: Downlink coordination

    Demonstrate the ability to autonomously schedule the downlink of known data using the SatNOGS network.



================
Success Criteria
================

.. success:: Minimum
    
    Test the Solar White Paint emissivity compared to AZ-93 over a time period of 6 months.


=========
Flow down
=========

* :doc:`solarwhite`

* :doc:`downlink`

